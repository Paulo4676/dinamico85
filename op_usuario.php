<?php

require_once('config.php');//mata! Aqui tem corage.
// Inserir usuario
if (isset($_POST['cadastro'])){
    $usu = new Usuario(
        $_POST['nome'],
        $_POST['email'],
        $_POST['login'],
        $_POST['senha']
    );
    $usu->insert();
    if($usu->getId()!=null){
        header('location:principal.php?link=12&msg=ok');
    }
    else{ header('location:principal.php?link=12&msg=erro');}
}







// excluir/deletar usuario
$id = filter_input(INPUT_GET,'id');
$excluir = filter_input(INPUT_GET,'excluir');
if(isset($id) && $excluir==1){
    $usuario = new Usuario();
    $usuario->setId($id);
    $usuario->delete();
    header('location:principal.php?link=13&msg=ok');
}




//Alterar o usuario
if (isset($_POST['alterar'])){
    $usuario = new Usuario();
    $usuario->update($_POST['id'],$_POST['nome'],$_POST['email'],$_POST['login']);
    header('location:principal.php?link=13&msg=ok');    
}




//sessao de usuario

if(isset($_POST['txt_login_usuario'])&&isset($_POST['logar_usuario'])){
    $txt_login = isset($_POST['txt_login_usuario'])?$_POST['txt_login_usuario']:'';
    $txt_senha = isset($_POST['txt_senha_usuario'])?$_POST['txt_senha_usuario']:'';
    //echo $txt_login.' - '.$txt_senha;
    if(empty($txt_login) || empty($txt_senha))  {
    
        header('location: logar_usuario.php?msg=preencha os dados de usuario');
        exit;
    }
    
    $usuario = new Usuario();
    
    $usuario->efetuarLogin($txt_login,$txt_senha);
    
    
    if(($usuario->getId()==null)){
    
        header('location: logar_usuario.php?msg=usuario ou senha invalidos');
    exit;
    }
    
    
    //registrando sessao de usuario
    $_SESSION['logado']= true;
    $_SESSION['id_usuario']= $usuario->getId();
    $_SESSION['nome_usuario']= $usuario->getNome();
    $_SESSION['login_usuario']= $usuario->getLogin();
    header('location: index.php?link=');
    }

    // encerrando a sessao
    
    if($_GET['sair']){
    $_SESSION['logado']= null;
    $_SESSION['id_usuario']= null;
    $_SESSION['nome_usuario']= null;
    $_SESSION['login_usuario']= null;
    $_SESSION['id_post']=null;
    $_SESSION['logado_usuario']=null;
    $_SESSION['id_adm']=null;
    $_SESSION['nome_adm']=null;
    $_SESSION['login_adm']=null;
    header('location:index.php');
    }

?>